import pig from '../../Assets/Imgs/Pig.svg'
import {Link} from 'react-router-dom'
import classes from './Error404.module.css'

export function Error404(){
    return(
        <>
            <main className={classes.main}>
                <h2 className={classes.erro}>Erro 404</h2>
                <img src={pig} alt="Imagem de porquinho perdido" />
                <p>Ops! Parece que o endereço que você digitou está incorreto...</p>
                <Link to="/">VOLTAR</Link>
            </main>
        </>
    )
}