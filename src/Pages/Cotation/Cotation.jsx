import classes from './Cotation.module.css'
import {api} from '../../Services/api'
import { useEffect, useState } from 'react'
import {CurrencyItem} from '../../Components/CurrencyItem/CurrencyItem'
import bubbles from '../../Assets/Imgs/Circles.svg'
import ret from '../../Assets/Imgs/rectangle.svg'

export function Cotation(){
    const [currency, setCurrency] = useState([])

    useEffect( () => {
        getCurrency()
    }, [])

    function getCurrency(){
        api.get('/')
        .then(res => setCurrency(res.data))
        .catch(err => console.log(err))
    }

    const currencies = Object.values(currency)

    return(
        <>
            <main className={classes.main}>
                <div className={classes.titleBox}>
                    <img src={ret} alt="Blue Rectangle" />
                    <p className={classes.title}>Cotação Moedas</p>
                    <img src={ret} alt="Blue Rectangle" />
                </div>
                <div>
                {
                    currencies.map(curr =>
                        <CurrencyItem
                            code={curr.code}
                            name={curr.name}
                            high={curr.high}
                            low={curr.low}
                            date={curr.create_date}
                        >
                        </CurrencyItem>
        
                    )
                }
                </div>
                <img className={classes.bubbles} src={bubbles} alt="Círculos estilizados" />
            </main>
        </>
    )
}