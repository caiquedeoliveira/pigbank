import construction from '../../Assets/Imgs/Construction.svg'
import {Link} from 'react-router-dom'
import classes from './InConstruction.module.css'


export function InConstruction(){
    return(
        <>
            <main className={classes.main}>
                <h1 className={classes.construction}>Em construção</h1>
                <img src={construction} alt="Imagem de construção" />
                <p>Parece que essa página ainda não foi implementada...</p>
                <p>Tente novamente mais tarde!</p>
                <Link to="/">VOLTAR</Link>
            </main>
        </>
    )
}