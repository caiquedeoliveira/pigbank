import {Link} from 'react-router-dom'
import classes from './Home.module.css'
import group1 from '../../Assets/Imgs/Group.svg'
import group2 from '../../Assets/Imgs/Group2.svg'
import bubbles from '../../Assets/Imgs/Circles.svg'
import ret from '../../Assets/Imgs/rectangle.svg'

export function Home(){
    return(
        <>
            <main className={classes.main}>
                <div className={classes.first}>
                    <div className={classes.pigPhoto}>
                        <img src={group1} alt="Pigbank first art" />
                    </div>
                    <div className={classes.pigText}>
                        <h1>A mais nova alternativa de banco digital chegou!</h1>
                        <p>Feito para caber no seu bolso e otimizar seu tempo. O PigBank veio pra ficar</p>
                        <Link to="access">Abra a sua conta</Link>
                    </div>
                </div>

                <div className={classes.second}>
                    <div className={classes.rectangle}>
                        <img src={ret} alt="Blue Rectangle" />
                        <h1>Fique por dentro!</h1>
                        <img src={ret} alt="Blue Rectangle" />
                    </div>

                    <p>Ao contrário do ditado popular, por aqui, quem se mistura com porco não come farelo! Conheça nossa plataforma exclusivamente dedicada para ampliar o seu patrimônio.</p>
                    <img src={group2} alt="Pigbank second art" />
                    <Link to="cotation">Cotação Moedas</Link>
                </div>
                <img className={classes.bubbles} src={bubbles} alt="Círculos estilizados" />
            </main>
        </>
    )
}