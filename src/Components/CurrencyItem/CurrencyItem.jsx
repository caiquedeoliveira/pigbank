import classes from './CurrencyItem.module.css'

export function CurrencyItem(props){
    return(
        <>
        <div className={classes.currency}>
            <div className={classes.currencyName}>
                <p>{props.name}</p>
                <p>{props.code}</p>
            </div>
            <div className={classes.currencyInfo}>
                <p>Máxima: {props.high}</p>
                <p>Mínima: {props.low}</p>
                <div className={classes.currencyUpdate}>
                    <p>Atualizado em {props.date}</p>
                </div>
            </div>
        </div>
        </>
    )
}