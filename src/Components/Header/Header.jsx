import {Link} from 'react-router-dom'
import classes from './Header.module.css'

export function Header(){
    return(
        <>
            <header  className={classes.header}>
                <div className={classes.logo}>
                    <Link to="/">Pigbank</Link>
                </div>
                <div className={classes.pages}>
                    <Link to="cotation">Cotação Moedas</Link>
                    <Link to="access">Acessar</Link>
                </div>
            </header>
        </>
    )
}