import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'

import {Header} from '../Components/Header/Header'
import {Footer} from '../Components/Footer/Footer'

import {Home} from '../Pages/Home/Home'
import { Error404 } from '../Pages/Error404/Error404'
import { InConstruction } from '../Pages/InConstruction/InConstruction'
import { Cotation } from '../Pages/Cotation/Cotation'

export function AppRoutes(){
    return(
        <Router>
            <Header />
            <Routes>
                <Route path={"/"} element={<Home/>}/>
                <Route path={"/cotation"} element={<Cotation/>}/>
                <Route path={"/access"} element={<InConstruction/>}/>
                <Route path={"*"} element={<Error404/>}/>
            </Routes>
            <Footer />
        </Router>
    )
}
